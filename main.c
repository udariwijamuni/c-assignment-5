#include <stdio.h>

#define PerformanceCost 500
#define perParticipant 3

//Functions
int NoOfParticipants(int a);
int Income(int a);
int Cost(int a);
int Profit(int a);

int NoOfParticipants(int a){
    return 120-(a-15)/5*20;

}

int Income(int a){
    return NoOfParticipants(a)*a;

}

int Cost(int a){
    return NoOfParticipants(a)*perParticipant+PerformanceCost;

}

int Profit(int a){
    return Income(a)-Cost(a);
}

int main(){
    int a;
    printf("\nExpected Profit for Ticket a: \n\n");
    for(a=5;a<50;a+=5)
    {
        printf("Ticket a = Rs.%d \t\t Profit = Rs.%d\n",a,Profit(a));
        printf("------------------------------------\n\n");

    }


        return 0;

}






